#!/bin/bash

# Check if path argument is provided
if [ $# -eq 0 ]; then
	echo "Usage: $0 path"
	exit 1
fi
path="$1"
# Check if path exists and is a directory
if [ ! -d "$path" ]; then
	echo "Error: Some problem with : '$path' is not a directory or does not exist"
	exit 1
fi
# Count the number of files and directories
number_of_files=$(find "$path" -maxdepth 1 -type f | wc -l)
number_of_directories=$(find "$path" -maxdepth 1 -type d | wc -l)

# Print the result
echo "Number of files in $path : $number_of_files"
echo "Number of directories in $path : $number_of_directories"
